from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import application, db, ma
from models import *

#endpoint para crear un nuevo enunciado.
@application.route("/enunciado", methods=["POST"])
@cross_origin()
def add_enunciado():
    id_pregunta = request.json["id_pregunta"]
    enunciado = request.json["enunciado"]

    new_enunciado = Enunciado(id_pregunta, enunciado)

    db.session.add(new_enunciado)
    db.session.commit()

    return enunciado_schema.jsonify(new_enunciado)


#endpoint para devolver todos los enunciados.
@application.route("/enunciado", methods=["GET"])
@cross_origin()
def get_enunciados():
    all_enunciados = Enunciado.query.all()
    if all_enunciados is None:
        return jsonify({})

    result = enunciados_schema.dump(all_enunciados)

    return jsonify(result.data)


#endpoint para devolver un solo enunciado por id.
@application.route("/enunciado/<id>", methods=["GET"])
@cross_origin()
def get_enunciado(id):
    enunciado = Enunciado.query.get(id)
    if enunciado is None:
        return jsonify({})

    return enunciado_schema.jsonify(enunciado)


#endpoint para actualizar un enunciado.
@application.route("/enunciado/<id>", methods=["PUT"])
@cross_origin()
def update_enunciado(id):
    enunciado = Enunciado.query.get(id)
    if enunciado is None:
        return jsonify({})

    if "id_pregunta" in request.json:
        pregunta.id_pregunta = request.json["id_pregunta"]
    if "enunciado" in request.json:
        pregunta.enunciado = request.json["enunciado"]

    db.session.commit()

    return enunciado_schema.jsonify(enunciado)


#endpoint para borrar un enunciado.
@application.route("/enunciado/<id>", methods=["DELETE"])
@cross_origin()
def delete_enunciado(id):
    enunciado = Enunciado.query.get(id)
    if enunciado is None:
        return jsonify({})

    db.session.delete(enunciado)
    db.session.commit()

    return enunciado_schema.jsonify(enunciado)
