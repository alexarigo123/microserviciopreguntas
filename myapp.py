from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

application = Flask(__name__)
cors = CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

# Se importan las configuraciones.
application.config.from_pyfile("config.py")

db = SQLAlchemy(application)
ma = Marshmallow(application)

# Se importan las vistas.
from tema_views import *
from pregunta_views import *
from enunciado_views import *
from respuesta_views import *
from estadistica_views import *

if __name__ == "__main__":
	application.run(host='0.0.0.0', port=5000)
