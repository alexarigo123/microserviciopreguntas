from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import application, db, ma
from models import *

#endpoint para crear una nueva pregunta.
@application.route("/pregunta", methods=["POST"])
@cross_origin()
def add_pregunta():
    nombre = request.json["nombre"]
    respuestas_correctas = request.json["respuestas_correctas"]
    respuestas_incorrectas = request.json["respuestas_incorrectas"]
    enunciados = request.json["enunciados"]

    new_pregunta = Pregunta(1, respuestas_correctas, respuestas_incorrectas, nombre)

    db.session.add(new_pregunta)
    db.session.commit()

    if enunciados is not None:
        print(enunciados)
        for enun in enunciados:
            enunciado = enun["enunciado"]
            respuestas = enun["respuestas"]
            if enunciado is not None:
                new_enunciado = Enunciado(new_pregunta.id, enunciado)
                db.session.add(new_enunciado)
                db.session.commit()
                for resp in respuestas:
                    respuesta = resp["respuesta"]
                    correcta = resp["correcta"]
                    db.session.add(Respuesta(new_enunciado.id, respuesta, correcta))
            db.session.commit()

    return pregunta_schema.jsonify(new_pregunta)


#endpoint para devolver todas las preguntas.
@application.route("/pregunta", methods=["GET"])
@cross_origin()
def get_preguntas():
    all_preguntas = Pregunta.query.all()
    if all_preguntas is None:
        return jsonify({})
    result = preguntas_schema.dump(all_preguntas)

    return jsonify(result.data)


#endpoint para devolver una sola pregunta por id.
@application.route("/pregunta/<id>", methods=["GET"])
@cross_origin()
def get_pregunta(id):
    pregunta = Pregunta.query.get(id)
    if pregunta is None:
        return jsonify({})

    return pregunta_schema.jsonify(pregunta)


#endpoint para devolver una sola pregunta por id con la información de sus enunciados y respuestas.
@application.route("/pregunta/<id>/all", methods=["GET"])
@cross_origin()
def get_pregunta_all(id):
    pregunta = Pregunta.query.get(id)

    if pregunta is None:
        return jsonify({})

    result_dict = {
        'tema': pregunta.tema.nombre,
        'respuestas_correctas': pregunta.respuestas_correctas,
        'respuestas_incorrectas': pregunta.respuestas_incorrectas,
        'nombre': pregunta.nombre,
        'enunciados': []
    }

    if pregunta.enunciados is not None:
        for enunciado in pregunta.enunciados:
            nuevo_enunciado = {'id': enunciado.id, 'enunciado': enunciado.enunciado, 'respuestas': []}

            if enunciado.respuestas is not None:
                for respuesta in enunciado.respuestas:
                    nueva_respuesta = {'id': respuesta.id, 'respuesta': respuesta.respuesta, 'correcta': respuesta.correcta }
                    nuevo_enunciado['respuestas'].append(nueva_respuesta)

            result_dict['enunciados'].append(nuevo_enunciado)

    return jsonify(result_dict)


#endpoint para actualizar una pregunta.
@application.route("/pregunta/<id>", methods=["PUT"])
@cross_origin()
def update_pregunta(id):
    pregunta = Pregunta.query.get(id)
    if pregunta is None:
        return jsonify({})

    if "nombre" in request.json:
        pregunta.nombre = request.json["nombre"]
    if "respuestas_correctas" in request.json:
        pregunta.respuestas_correctas = request.json["respuestas_correctas"]
    if "respuestas_incorrectas" in request.json:
        pregunta.respuestas_incorrectas = request.json["respuestas_incorrectas"]

    db.session.commit()

    if "enunciados" in request.json:
        enunciados = request.json["enunciados"]
        for enun in enunciados:
            if enun["id"] == 0:
                enunciado = enun["enunciado"]
                respuestas = enun["respuestas"]
                if enunciado:
                    new_enunciado = Enunciado(pregunta.id, enunciado)
                    db.session.add(new_enunciado)
                    db.session.commit()
                    for resp in respuestas:
                        respuesta = resp["respuesta"]
                        correcta = resp["correcta"]
                        db.session.add(Respuesta(new_enunciado.id, respuesta, correcta))
            else:
                edit_enunciado = Enunciado.query.get(enun["id"])
                if edit_enunciado is not None:
                    if "enunciado" in enun:
                        edit_enunciado.enunciado = enun["enunciado"]
                    if "respuestas" in enun:
                        respuestas = enun["respuestas"]
                        for resp in respuestas:
                            if resp["id"] == 0:
                                respuesta = resp["respuesta"]
                                correcta = resp["correcta"]
                                db.session.add(Respuesta(edit_enunciado.id, respuesta, correcta))
                            else:
                                edit_respuesta = Respuesta.query.get(enun["id"])
                                if edit_respuesta is not None:
                                    if "respuesta" in resp:
                                        edit_respuesta.respuesta = resp["respuesta"]
                                    if "correcta" in resp:
                                        edit_respuesta.correcta = resp["correcta"]

            db.session.commit()

    return pregunta_schema.jsonify(pregunta)


#endpoint para borrar una pregunta.
@application.route("/pregunta/<id>", methods=["DELETE"])
@cross_origin()
def delete_pregunta(id):
    pregunta = Pregunta.query.get(id)
    if pregunta is None:
        return jsonify({})

    if pregunta.enunciados is not None:
        for enunciado in pregunta.enunciados:
            if enunciado.respuestas is not None:
                for respuesta in enunciado.respuestas:
                    db.session.delete(respuesta)
                    db.session.commit()
            db.session.delete(enunciado)
            db.session.commit()

    estadistica = Estadistica.query.filter(Estadistica.id_pregunta == id).first()
    if estadistica is not None:
        db.session.delete(estadistica)
        db.session.commit()

    db.session.delete(pregunta)
    db.session.commit()

    return pregunta_schema.jsonify(pregunta)
