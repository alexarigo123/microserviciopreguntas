from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import application, db, ma
from models import *

#endpoint para crear un nuevo tema.
@application.route("/tema", methods=["POST"])
@cross_origin()
def add_tema():
    nombre = request.json["nombre"]

    new_tema = Tema(nombre)

    db.session.add(new_tema)
    db.session.commit()

    return tema_schema.jsonify(new_tema)


#endpoint para devolver todos los temas.
@application.route("/tema", methods=["GET"])
@cross_origin()
def get_temas():
    all_temas = Tema.query.all()
    if all_temas is None:
        return jsonify({})

    result = temas_schema.dump(all_temas)

    return jsonify(result.data)


#endpoint para devolver un solo tema por id.
@application.route("/tema/<id>", methods=["GET"])
@cross_origin()
def get_tema(id):
    tema = Tema.query.get(id)
    if tema is None:
        return jsonify({})

    return tema_schema.jsonify(tema)


#endpoint para actualizar un tema.
@application.route("/tema/<id>", methods=["PUT"])
@cross_origin()
def update_tema(id):
    tema = Tema.query.get(id)
    if tema is None:
        return jsonify({})

    nombre = request.json["nombre"]
    
    tema.nombre = nombre

    db.session.commit()

    return tema_schema.jsonify(tema)


#endpoint para borrar un tema.
@application.route("/tema/<id>", methods=["DELETE"])
@cross_origin()
def delete_tema(id):
    tema = Tema.query.get(id)
    if tema is None:
        return jsonify({})

    db.session.delete(tema)
    db.session.commit()

    return tema_schema.jsonify(tema)
