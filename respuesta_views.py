from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import application, db, ma
from models import *

#endpoint para crear un nuevo respuesta.
@application.route("/respuesta", methods=["POST"])
@cross_origin()
def add_respuesta():
    id_enunciado = request.json["id_enunciado"]
    respuesta = request.json["respuesta"]
    correcta = request.json["correcta"]

    new_respuesta = Respuesta(id_enunciado, respuesta, correcta)

    db.session.add(new_respuesta)
    db.session.commit()

    return respuesta_schema.jsonify(new_respuesta)


#endpoint para devolver todos los respuestas.
@application.route("/respuesta", methods=["GET"])
@cross_origin()
def get_respuestas():
    all_respuestas = Respuesta.query.all()
    if all_respuestas is None:
        return jsonify({})

    result = respuestas_schema.dump(all_respuestas)

    return jsonify(result.data)


#endpoint para devolver un solo respuesta por id.
@application.route("/respuesta/<id>", methods=["GET"])
@cross_origin()
def get_respuesta(id):
    respuesta = Respuesta.query.get(id)
    if respuesta is None:
        return jsonify({})

    return respuesta_schema.jsonify(respuesta)


#endpoint para actulizar un respuesta.
@application.route("/respuesta/<id>", methods=["PUT"])
@cross_origin()
def update_respuesta(id):
    respuesta = Respuesta.query.get(id)
    if respuesta is None:
        return jsonify({})

    if "id_enunciado" in request.json:
        respuesta.id_enunciado = request.json["id_enunciado"]
    if "respuesta" in request.json:
        respuesta.respuesta = request.json["respuesta"]
    if "correcta" in request.json:
        respuesta.correcta = request.json["correcta"]

    db.session.commit()

    return respuesta_schema.jsonify(respuesta)


#endpoint para borrar un respuesta.
@application.route("/respuesta/<id>", methods=["DELETE"])
@cross_origin()
def delete_respuesta(id):
    respuesta = Respuesta.query.get(id)
    if respuesta is None:
        return jsonify({})

    db.session.delete(respuesta)
    db.session.commit()

    return respuesta_schema.jsonify(respuesta)
