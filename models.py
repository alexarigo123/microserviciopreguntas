from myapp import db, ma
from sqlalchemy.sql import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

# Modelo de la tabla Tema.
class Tema(db.Model):
    __tablename__ = 'tema'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100), unique=True, nullable=False)
    preguntas = relationship("Pregunta", back_populates="tema")
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, nombre):
        self.nombre = nombre


# Esquema para exportar como JSON entidades de la tabla Tema.
class TemaSchema(ma.Schema):
    class Meta:
        fields = ('id', 'nombre')


tema_schema = TemaSchema()
temas_schema = TemaSchema(many=True)


# Modelo de la tabla Pregunta.
class Pregunta(db.Model):
    __tablename__ = 'pregunta'

    id = db.Column(db.Integer, primary_key=True)
    id_tema = db.Column(db.Integer, ForeignKey('tema.id'), nullable=False)
    tema = relationship("Tema", back_populates="preguntas")
    estadistica = relationship("Estadistica", back_populates="pregunta")
    respuestas_correctas = db.Column(db.Integer, nullable=False)
    respuestas_incorrectas = db.Column(db.Integer, nullable=False)
    enunciados = relationship("Enunciado", back_populates="pregunta")
    nombre = db.Column(db.String(100), nullable=True)
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, id_tema, respuestas_correctas, respuestas_incorrectas, nombre):
        self.id_tema = id_tema
        self.respuestas_correctas = respuestas_correctas
        self.respuestas_incorrectas = respuestas_incorrectas
        self.nombre = nombre


# Esquema para exportar como JSON entidades de la tabla Pregunta.
class PreguntaSchema(ma.Schema):
    class Meta:
        fields = ('id', 'id_tema', 'respuestas_correctas', 'respuestas_incorrectas', 'nombre')


pregunta_schema = PreguntaSchema()
preguntas_schema = PreguntaSchema(many=True)


# Modelo de la tabla Enunciado.
class Enunciado(db.Model):
    __tablename__ = 'enunciado'

    id = db.Column(db.Integer, primary_key=True)
    id_pregunta = db.Column(db.Integer, ForeignKey('pregunta.id'), nullable=False)
    pregunta = relationship("Pregunta", back_populates="enunciados")
    enunciado = db.Column(db.String(4000), nullable=False)
    respuestas = relationship("Respuesta", back_populates="enunciado")
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, id_pregunta, enunciado):
        self.id_pregunta = id_pregunta
        self.enunciado = enunciado


# Esquema para exportar como JSON entidades de la tabla Enunciado.
class EnunciadoSchema(ma.Schema):
    class Meta:
        fields = ('id', 'id_pregunta', 'enunciado')


enunciado_schema = EnunciadoSchema()
enunciados_schema = EnunciadoSchema(many=True)


# Modelo de la tabla Respuesta.
class Respuesta(db.Model):
    __tablename__ = 'respuesta_enunciado'

    id = db.Column(db.Integer, primary_key=True)
    id_enunciado = db.Column(db.Integer, ForeignKey('enunciado.id'), nullable=False)
    enunciado = relationship("Enunciado", back_populates="respuestas")
    respuesta = db.Column(db.String(4000), nullable=False)
    correcta = db.Column(db.Boolean, nullable=False)
    fecha_creacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    fecha_actualizacion = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now(), onupdate=func.now())

    def __init__(self, id_enunciado, respuesta, correcta):
        self.id_enunciado = id_enunciado
        self.respuesta = respuesta
        self.correcta = correcta


# Esquema para exportar como JSON entidades de la tabla Respuesta.
class RespuestaSchema(ma.Schema):
    class Meta:
        fields = ('id', 'id_enunciado', 'respuesta', 'correcta')


respuesta_schema = RespuestaSchema()
respuestas_schema = RespuestaSchema(many=True)


# Modelo de la tabla Estadistica.
class Estadistica(db.Model):
    __tablename__ = 'estadistica'

    id = db.Column(db.Integer, primary_key=True)
    id_pregunta = db.Column(db.Integer, ForeignKey('pregunta.id'), nullable=False)
    pregunta = relationship("Pregunta", back_populates="estadistica")
    correctas = db.Column(db.Integer, nullable=False)
    incorrectas = db.Column(db.Integer, nullable=False)

    def __init__(self, id_pregunta, correctas, incorrectas):
        self.id_pregunta = id_pregunta
        self.correctas = correctas
        self.incorrectas = incorrectas


# Esquema para exportar como JSON entidades de la tabla Estadistica.
class EstadisticaSchema(ma.Schema):
    class Meta:
        fields = ('id_pregunta', 'correctas', 'incorrectas')


estadistica_schema = EstadisticaSchema()
estadisticas_schema = EstadisticaSchema(many=True)
