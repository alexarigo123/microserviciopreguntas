from flask import jsonify, request
from flask_cors import CORS, cross_origin
from myapp import application, db, ma
from models import *

#endpoint para crear un nuevo estadistica.
@application.route("/estadistica", methods=["POST"])
@cross_origin()
def add_estadistica():
    id_pregunta = request.json["id_pregunta"]
    correctas = request.json["correctas"]
    incorrectas = request.json["incorrectas"]

    estadistica = Estadistica.query.filter(Estadistica.id_pregunta == id_pregunta).first()
    if estadistica is None:
        new_estadistica = Estadistica(id_pregunta, correctas, incorrectas)
        db.session.add(new_estadistica)
    else:
        estadistica.correctas += correctas
        estadistica.incorrectas += incorrectas
    
    db.session.commit()

    return estadistica_schema.jsonify(new_estadistica if estadistica is None else estadistica)


#endpoint para devolver todos los estadisticas.
@application.route("/estadistica", methods=["GET"])
@cross_origin()
def get_estadisticas():
    all_estadisticas = Estadistica.query.all()
    if all_estadisticas is None:
        return jsonify({})

    result = estadisticas_schema.dump(all_estadisticas)

    return jsonify(result.data)


#endpoint para devolver un solo estadistica por id pregunta.
@application.route("/estadistica/pregunta/<id_pregunta>", methods=["GET"])
@cross_origin()
def get_estadistica_by_pregunta(id_pregunta):
    estadistica = Estadistica.query.filter(Estadistica.id_pregunta == id_pregunta).first()
    if estadistica is None:
        return jsonify({})

    return estadistica_schema.jsonify(estadistica)
